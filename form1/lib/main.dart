import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Form';
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text(appTitle)),
        body: MyCustomForm(),
      )
    );
  }
}

class MyCustomForm extends StatefulWidget {
  MyCustomForm({Key? key}) : super(key: key);

  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      //autovalidateMode: AutovalidateMode.always,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            child: TextFormField(
            decoration: InputDecoration(
              hintText: 'Enter some text',
              border: OutlineInputBorder()
            ),
            validator: (value) {
              if(value==null || value.isEmpty) {
                return 'Please enter text';
              }
              return null;
            },
          ),
          padding: EdgeInsets.all(16),
          ),
          Padding(
            child:  TextFormField(
            decoration: InputDecoration(
              labelText: 'Enter Email here',
              helperText: 'example@hotmail.com',
              border: UnderlineInputBorder()
            ),
            validator: (value) => EmailValidator.validate(value!)? null : 'Please enter Email'
          ),
          padding: EdgeInsets.all(16),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: SizedBox(
            width: double.infinity,
            height: 30,
            child: ElevatedButton(
            onPressed: (){
              if(_formKey.currentState!.validate()){
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Processing'))
                );
              }
            }, 
            child: const Text('Submit')
            ),
          ),)
          ,
          
        ],
      ),
    );
  }
}